// flow-typed signature: 2c44de4e79655b696c4ccf16bda9f822
// flow-typed version: <<STUB>>/location-autocomplete_v^1.2.4/flow_v0.86.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'location-autocomplete'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'location-autocomplete' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'location-autocomplete/index' {
  declare module.exports: $Exports<'location-autocomplete'>;
}
declare module 'location-autocomplete/index.js' {
  declare module.exports: $Exports<'location-autocomplete'>;
}
