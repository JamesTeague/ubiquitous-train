// @flow

import uuid from 'uuid';
import type { DataSnapshot } from '@firebase/database';
import {
  bidsRef, usersRef, votesRef,
} from '../config/firebase';
import type { City, User } from '../types';


export const onceGetBidByCity = (cityId: string): DataSnapshot => bidsRef.orderByChild('cityId').equalTo(cityId).once('value');
export const onceGetBid = (bidId: string): DataSnapshot => bidsRef.child(bidId).once('value');
export const onceGetUser = (uid: string): DataSnapshot => usersRef.child(uid).once('value');

export const doesUserExist = async (uid: string) => {
  const firebaseUser = await onceGetUser(uid);
  return firebaseUser.exists() ? firebaseUser.val() : false;
};

export const doesBidExist = async (cityId: string): Promise<boolean> => {
  const bid = await onceGetBidByCity(cityId);
  return bid.exists();
};

export const deleteBid = async (bidId: string, userUid: string) => {
  const bidSnapshot = await onceGetBid(bidId);
  const bid = bidSnapshot.val();
  if (bid.userId === userUid) {
    return bidsRef.child(bidId).remove();
  }
};

export const getUserBidCount = async (userId: string) => {
  const bids = await bidsRef
    .orderByChild('userId')
    .equalTo(userId)
    .once('value');
  return bids.val() ? Object.keys(bids.val()).length : 0;
};

export const doCreateUser = (user: User) =>
  usersRef
    .child(user.uid)
    .set(user);

export const doCreateBid = async (place: City, user: User): Promise<void> => {
  const bidId = uuid();
  return bidsRef.child(bidId).set({
    id: bidId,
    city: place,
    cityId: place.id,
    userId: user.uid,
  });
};

export const adjustUserPoints = (uid: string, points: number) =>
  usersRef
    .child(`${uid}/points`)
    .set(points);

export const doCreateVote = async (user: User, bidId: string, points: number) => {
  if (user.points > 0 && points <= user.points) {
    const voteId = uuid();
    await votesRef
      .child(voteId)
      .set({
        bidId,
        points,
        userId: user.uid,
      });
    adjustUserPoints(user.uid, user.points - points);
  }
};
