// @flow
import * as ramda from 'ramda';
import type { LocationAutocomplete } from 'location-autocomplete';

import { getUserBidCount } from '../firebase/database';
import type { City, Permission, User } from '../types';

const byPropKey = (propertyName: string, value: any) => () => ({
  [propertyName]: value,
});

const formatPlace = (place: LocationAutocomplete): City => ({
  name: place.address_components
    .filter(component => component.types.includes('locality'))
    .map(component => ({ cityName: component.long_name }))[0]
    .cityName,
  state: place.address_components
    .filter(component => component.types.includes('administrative_area_level_1'))
    .map(component => ({ name: component.long_name, abbreviation: component.short_name }))[0],
  country: place.address_components
    .filter(component => component.types.includes('country'))
    .map(component => ({ name: component.long_name, abbreviation: component.short_name }))[0],
  address: place.formatted_address.replace(/( \d{5})/, ''),
  url: place.url,
  id: place.place_id,
});

const parseToInt = (value: string, defaultValue: *): number => {
  const number = parseInt(value);
  return isNaN(number) ? defaultValue : number;
};

const hasPermission = (permission: Permission, user: User): boolean =>
  user.permission >= permission.ordinal;

const canSubmitBid = async (user: User) => {
  const bidCount = await getUserBidCount(user.uid);
  return bidCount < 5;
};

const sortByProperty = (arrayOfObjects: Array<Object>, property: string): Array<Object> =>
  ramda.sortBy(ramda.compose(ramda.toLower, ramda.prop(property)))(arrayOfObjects);

export default {
  byPropKey,
  canSubmitBid,
  sortByProperty,
  hasPermission,
  formatPlace,
  parseToInt,
};
