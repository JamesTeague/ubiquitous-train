import React from 'react';
import { Table } from 'reactstrap';
import BidListItem from './BidListItem';

const BidList = ({ bids, onClickCallback }) => (
  <Table responsive striped>
    <thead>
      <tr>
        <th>Name</th>
        <th>State</th>
        <th>Country</th>
      </tr>
    </thead>
    <tbody>
      {bids.map(bid => (
        <BidListItem key={bid.id} onClickCallback={onClickCallback} bid={bid} />
      ))}
    </tbody>
  </Table>
);

export default BidList;
