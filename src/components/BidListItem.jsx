import React from 'react';

const BidListItem = ({ bid, onClickCallback }) => (
  <tr onClick={() => onClickCallback(bid.id)}>
    <td>{bid.city.name}</td>
    <td>{bid.city.state.abbreviation}</td>
    <td>{bid.city.country.abbreviation}</td>
    <td style={{ display: 'none' }}>{bid.id}</td>
  </tr>
);

export default BidListItem;
