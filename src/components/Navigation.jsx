import React from 'react';
import { connect } from 'react-redux';
import { Collapse, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink, } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import util from '../util';
import * as routes from '../constants/routes';
import SignOutLink from './SignOutLink';
import permissions from '../constants/permissions';

class Navigation extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.redirectTo = this.redirectTo.bind(this);
    this.state = {
      isOpen: false,
    };
  }

  toggle() {
    const { isOpen } = this.state;
    this.setState({
      isOpen: !isOpen,
    });
  }

  redirectTo(route) {
    const { history } = this.props;
    history.push(route);
    this.toggle();
  }

  render() {
    const { authenticatedUser } = this.props;
    const { isOpen } = this.state;
    return (
      <NavigationContainer
        authUser={authenticatedUser}
        toggle={this.toggle}
        isOpen={isOpen}
        redirectTo={this.redirectTo}
      />
    );
  }
}

const NavigationContainer = ({
  authUser, toggle, isOpen, redirectTo,
}) => (
  <Navbar color={'dark'} dark expand={'md'} sticky={'top'}>
    <NavbarBrand href={''} onClick={() => redirectTo(routes.HOME)}>Retreat</NavbarBrand>
    <NavbarToggler onClick={toggle} />
    <Collapse isOpen={isOpen} navbar>
      {
        authUser
          ? <NavigationAuth redirectTo={redirectTo} user={authUser} />
          : <NavigationNonAuth redirectTo={redirectTo} />}
    </Collapse>
  </Navbar>
);

const NavigationAuth = ({ redirectTo, user }) => (
  <Nav className={'ml-auto'} navbar>
    { util.hasPermission(permissions.get('VOTER'), user)
      && <NavItem><NavLink onClick={() => redirectTo(routes.BIDS)}>Bids</NavLink></NavItem>
    }
    <NavItem><NavLink onClick={() => redirectTo(routes.ACCOUNT)}>Account</NavLink></NavItem>
    <NavItem><SignOutLink /></NavItem>
  </Nav>
);

const NavigationNonAuth = ({ redirectTo }) => (
  <Nav className={'ml-auto'} navbar>
    <NavItem><NavLink onClick={() => redirectTo(routes.LOGIN)}>Sign In</NavLink></NavItem>
  </Nav>
);

const mapStateToProps = state => ({
  authenticatedUser: state.session.authUser,
});

export default withRouter(connect(mapStateToProps)(Navigation));
