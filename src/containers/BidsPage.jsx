import React, { Component } from 'react';
import { Alert, Button, Col, Container, InputGroup, InputGroupAddon, ListGroup, ListGroupItem, Row, } from 'reactstrap';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import { firebaseConnect } from 'react-redux-firebase';
import LocationAutocomplete from 'location-autocomplete';
import Util from '../util';
import { GOOGLE_PLACES_API_KEY } from '../config/dev';
import { deleteBid, doCreateBid, doesBidExist } from '../firebase/database';
import withAuthorization from '../hoc/withAuthorization';
import firebasePaths from '../constants/referencePaths';

class BidsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bidMessage: null,
      place: null,
    };
  }

  showBidMessage(color, message) {
    this.setState(Util.byPropKey('bidMessage', { color, message }));
    setTimeout(() => this.setState(Util.byPropKey('bidMessage', null)), 3000);
  }

  render() {
    const { bidMessage } = this.state;
    const { bids, user } = this.props;
    return (
      <Container className={'pt-5'}>
        <Row>
          <Col lg={{ size: 8, offset: 2 }}>
            <InputGroup>
              <LocationAutocomplete
                name={'city'}
                placeholder={'City Name'}
                className={'form-control'}
                locationType={'(cities)'}
                googleAPIKey={GOOGLE_PLACES_API_KEY}
                componentRestrictions={{ country: ['us', 'ca', 'mx', 'jm', 'pr'] }}
                onChange={() => {}}
                onDropdownSelect={(component) => {
                  this.setState(
                    Util.byPropKey('place', Util.formatPlace(component.autocomplete.getPlace())),
                  );
                }}
              />
              <InputGroupAddon addonType={'append'}>
                <Button
                  className={'btn btn-success'}
                  onClick={async () => {
                    const { place } = this.state;
                    if (!place) {
                      return;
                    }
                    if (await Util.canSubmitBid(user)) {
                      if (!(await doesBidExist(place.id))) {
                        doCreateBid(place, user)
                          .then(() => this.showBidMessage('success', 'Bid created.'))
                          .catch(reason => console.log('Class: BidsPage, Function: , Line 52 reason(): ', reason));
                      }
                      else {
                        this.showBidMessage('warning', 'This city has already been submitted.');
                      }
                    }
                    else {
                      this.showBidMessage('warning', 'You have reached your bid limit.');
                    }
                  }}
                >
                  Add Bid
                </Button>
              </InputGroupAddon>
            </InputGroup>
            {bidMessage && (
              <Col sm={12}>
                <Alert color={bidMessage.color}>
                  {bidMessage.message}
                </Alert>
              </Col>)}
          </Col>
        </Row>
        <br />
        <br />
        <Row>
          <Col lg={{ size: 8, offset: 2 }}>
            <ListGroup flush>
              {bids && Object.values(bids).map(bid => (
                <ListGroupItem key={bid.id}>
                  {bid.city.address}
                  <Button close onClick={() => deleteBid(bid.id, user.uid)} />
                </ListGroupItem>
              ))}
            </ListGroup>
          </Col>
        </Row>
      </Container>
    );
  }
}

const firebaseConnection = firebaseConnect(() => [
  { path: firebasePaths.BIDS },
]);

const mapStateToProps = state => ({
  bids: state.firebase.data.bids,
  user: state.session.authUser,
});

const authenticationCondition = authenticatedUser => !!authenticatedUser;

export default compose(
  withAuthorization(authenticationCondition),
  firebaseConnection,
  connect(mapStateToProps),
)(BidsPage);
