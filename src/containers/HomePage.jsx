import React, { Component } from 'react';
import { firebaseConnect, isEmpty, isLoaded } from 'react-redux-firebase';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import BidList from '../components/BidList';
import withAuthorization from '../hoc/withAuthorization';
import firebasePaths from '../constants/referencePaths';
import { BID_INFO } from '../constants/routes';


/* eslint-disable react/prefer-stateless-function */
class HomePage extends Component {
  redirectToBidDetail = (id) => {
    const { history } = this.props;
    history.push(`${BID_INFO}/${id}`);
  };

  render() {
    const { bids } = this.props;
    if (isLoaded(bids) && !isEmpty(bids)) {
      return (
        <BidList
          bids={Object.values(bids)}
          onClickCallback={this.redirectToBidDetail}
        />
      );
    }
    return <div>No data retrieved.</div>;
  }
}

const firebaseConnection = firebaseConnect(() => [
  { path: firebasePaths.BIDS },
]);

const mapStateToProps = state => ({
  bids: state.firebase.data.bids,
});

const authenticationCondition = authenticatedUser => !!authenticatedUser;

export default compose(
  withAuthorization(authenticationCondition),
  firebaseConnection,
  withRouter,
  connect(mapStateToProps),
)(HomePage);
