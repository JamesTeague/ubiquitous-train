import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';
import { Button, Col, Container, Input, InputGroup, InputGroupAddon, Jumbotron, Row } from 'reactstrap';
import Util from '../util';
import { doCreateVote, onceGetBid } from '../firebase/database';
import withAuthorization from '../hoc/withAuthorization';
import PermissionEnum from '../constants/permissions'

class BidInfoPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      /* eslint-disable-next-line */
      bid: null,
      bidId: this.props.match.params.bidId,
      voteValue: 0,
    };
  }

  async componentDidMount() {
    const { bidId } = this.state;
    onceGetBid(bidId).then((bid => this.setState(Util.byPropKey('bid', bid.val()))));
  }

  render() {
    const { bid } = this.state;
    const { user } = this.props;
    if(!bid) return <div>Loading...</div>;
    return (
      <Container>
        <h3>{bid.city.address}</h3>
        <Row>
          <Col>
            <Row>
              <Col>
                <Jumbotron>
                  <h4>Weather Information</h4>
                  Coming Soon...
                </Jumbotron>
              </Col>
            </Row>
            <Row>
              <Col>
                <Jumbotron>
                  <h4>Flight Information</h4>
                  Coming Soon...
                </Jumbotron>
              </Col>
            </Row>
          </Col>
            {Util.hasPermission(PermissionEnum.get('VOTER'), user)
            && <Col sm={'2'}>
                <VoteForm
                state={this.state}
                props={this.props}
                onChange={event => this.setState(
                  Util.byPropKey('voteValue', Util.parseToInt(event.target.value, '')),
                )}
              />
            </Col>}
        </Row>
      </Container>
    );
  }
}

const VoteForm = ({ state, props, onChange }) => {
  const { voteValue, bidId } = state;
  const { user } = props;
  return (
    <InputGroup>
      <Input
        className={'form-control'}
        type={'select'}
        value={voteValue}
        children={[...Array(user.points+1).keys()].map((point) => (
          <option key={point}>{point}</option>
        ))}
        onChange={onChange}
      />
      <InputGroupAddon addonType={'append'}>
          <Button
            className={'btn btn-success'}
            onClick={() => doCreateVote(user, bidId, voteValue)}
          >
            Vote
          </Button>
      </InputGroupAddon>
    </InputGroup>
  )
};

const mapStateToProps = state => ({
  user: state.session.authUser,
});

const authenticationCondition = authenticatedUser => !!authenticatedUser;

export default compose(
  withAuthorization(authenticationCondition),
  withRouter,
  connect(mapStateToProps),
)(BidInfoPage);
