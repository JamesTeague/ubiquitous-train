// @flow

export type Country = {
  abbreviation: string,
  name: string,
}

export type State = Country;

export type City = {
  address: string,
  country: Country,
  id: string,
  name: string,
  state: State,
  url: string,
}

export type Bid = {
  city: City,
  cityId: string,
  userId: string,
}

export type FirebaseBid = Bid & {
  key: string,
}

export type User = {
  displayName: string,
  email: string,
  emailVerified: boolean,
  permission: number,
  points: number,
  uid: string,
}

export type Vote = {
  bidId: string,
  points: number,
  userId: string,
}

export type Permission = {
  ordinal: number,
  string: string,
}
