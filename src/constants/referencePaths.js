const firebasePaths = {
  BIDS: 'bids',
  USERS: 'users',
  VOTES: 'votes',
};

export default firebasePaths;
