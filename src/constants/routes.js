export const ACCOUNT = '/account';
export const BIDS = '/bids';
export const BID_INFO = '/bidInfo';
export const HOME = '/home';
export const INDEX = '/';
export const LOGIN = '/login';
