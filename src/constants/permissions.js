// @flow
import { Map } from 'immutable';
import type { Permission } from '../types';

const PermissionEnum: Map<string, Permission> = Map({
  USER: { ordinal: 0, string: 'User' },
  VOTER: { ordinal: 1, string: 'Voter' },
  ADMIN: { ordinal: 2, string: 'Admin' },
});

export default PermissionEnum;
