import React from 'react';
import { connect } from 'react-redux';
import { authRef, usersRef } from '../config/firebase';
import { AUTH_USER_SET } from '../actions/types';
import { doesUserExist } from '../firebase/database';

const withAuthentication = (Component) => {
  class WithAuthentication extends React.Component {
    async componentDidMount() {
      const { onSetAuthUser } = this.props;
      let authUserRef;

      authRef.onAuthStateChanged((authUser) => {
        if (authUser) {
          doesUserExist(authUser.uid).then((user) => {
            /*
            * Listener intended to update when user votes.
            * I am not sure if this is the best solution,
            * but it is the one with the smallest footprint that guarantees db consistency.
            */
            authUserRef = usersRef.child(authUser.uid);
            authUserRef.on('value', sessionUser => onSetAuthUser(sessionUser.val()));
            if (user) onSetAuthUser(user);
            else onSetAuthUser(authUser);
          });
        }
        else {
          onSetAuthUser(null);
          if (authUserRef) authUserRef.off('value');
        }
      });
    }

    render() {
      return (
        <Component />
      );
    }
  }

  const mapDispatchToProps = dispatch => ({
    onSetAuthUser: authUser => dispatch(AUTH_USER_SET(authUser)),
  });

  return connect(null, mapDispatchToProps)(WithAuthentication);
};

export default withAuthentication;
